
/* Oppgave 2 (i)*/

SELECT navn, personnr, adr, bolignr
FROM Salgspart, Boligsalg, Person
GROUP BY navn, personnr, adr, bolignr
HAVING count(distinct(salgsrolle)) <= 3;


/* Oppgave 2 (ii)*/

/* Denne funker ikke helt. Det vil være flere
 * forekomster hvis boligen er solgt flere ganger uten 
 * boligtypen*/

CREATE VIEW maksAntallEndringer AS
SELECT DISTINCT (mnr), count(boligtype) AS teller
FROM Boligsalg
GROUP BY mnr
ORDER BY teller ASC;

SELECT mnr, teller
FROM maksAntallEndringer
WHERE teller = (
	SELECT MAX(teller)
	FROM maksAntallEndringer);


/* Alternativ 2 */

SELECT b1.mnr, count(b2.boligtype) AS antall
FROM Boligsalg b1 JOIN Boligsalg b2 ON mnr
WHERE b1.salgsdato < b2.salgsdato
AND b1.boligtype != b2.boligtype
AND NOT EXISTS (
	SELECT *
	FROM Boligsalg b3
	WHERE b3.salgsdato > b1.salgsdato
	AND b3.salgsdato < b2.salgsdato)
GROUP BY b1.mnr;


/* Oppgave 2(iii)*/
/* Alternativ 1*/

SELECT bnr, knr, gnr, fnr, snr
FROM Matrikkel 
WHERE mnr IN (
	SELECT mnr
	FROM Boligsalg JOIN Salgspart
	GROUP BY mnr
	HAVING distinct(salgsnr) count(salgspart) < 3);

/* Alternativ 2*/
SELECT bnr, knr, gnr, fnr, snr
FROM Matrikkel 
WHERE mnr NOT IN (
	SELECT mnr
	FROM Boligsalg JOIN Salgspart
	WHERE salgsrolle LIKE 'megler');







