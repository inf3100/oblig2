Oblig 2
=======

*Av Andreas L. Truchs og Vegard D. Søyseth*

[Besvarelse](https://bytebucket.org/inf3100/oblig2/raw/1fe5181c5426fdb75c045e6ce10ab8682c0f5836/oblig2-andrelt-vegardds.pdf?token=58c945d1b4309ef9c756edc0a925f162f797a0e2)

[Tilbakemelding fra retter](https://devilry.ifi.uio.no/devilry_student/group/200928/deliveries/delivery/158222)

[Oppgave tekst](http://www.uio.no/studier/emner/matnat/ifi/INF3100/v15/undervisningsmateriale/obliger/oblig2.pdf)

[Oppskriftsbok FDer, MVDer og relasjonsalgebra](http://www.uio.no/studier/emner/matnat/ifi/INF3100/v15/undervisningsmateriale/forelesningsmateriale/oppskriftsbok.pdf)

